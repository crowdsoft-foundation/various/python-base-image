FROM python:3-alpine as base
RUN pip install --upgrade pip

RUN mkdir /svc
COPY . /svc
WORKDIR /svc

RUN apk add --update \
            --repository http://dl-cdn.alpinelinux.org/alpine/edge/main \
       alpine-sdk \
       gcc \
       musl-dev \
       linux-headers \
       libffi-dev \
       openssl-dev \
       jpeg-dev \
       zlib-dev \
       freetype-dev \
       lcms2-dev \
       openjpeg-dev\
       tiff-dev \
       tk-dev \
       tcl-dev \
       python3-dev \
       build-base \
       libxml2-dev \
       libxslt-dev \
       swig

RUN pip install -r ./requirements.txt --user


FROM python:3-alpine
ENV PYTHONUNBUFFERED 1
RUN pip install --upgrade pip

RUN apk add --update \
            --repository http://dl-cdn.alpinelinux.org/alpine/edge/main \
       openssl-dev

COPY --from=base /root/.local /root/.local

CMD ["/bin/sh"]